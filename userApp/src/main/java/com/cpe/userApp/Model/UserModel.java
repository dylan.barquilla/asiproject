package com.cpe.userApp.Model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserModel implements Serializable {

    private static final long serialVersionUID = 2733795832476568049L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String login;
    private String pwd;
    private float account;
    private String lastName;
    private String surName;
    private String email;


    public UserModel() {
        this.login = "";
        this.pwd = "";
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public UserModel(String login, String pwd) {
        super();
        this.login = login;
        this.pwd = pwd;
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    /*
    public Set<CardDto> getCardList() {
        return cardList;
    }

    public void setCardList(Set<CardDto> cardList) {
        this.cardList = cardList;
    }

    public void addAllCardList(Collection<CardDto> cardList) {
        this.cardList.addAll(cardList);
    }


    public void addCard(CardDto card) {
        this.cardList.add(card);
        card.setUser(this.id);
    }

    private boolean checkIfCard(CardDto c) {
        for (CardDto c_c : this.cardList) {
            if (c_c.getId() == c.getId()) {
                return true;
            }
        }
        return false;
    }

     */

    public float getAccount() {
        return account;
    }

    public void setAccount(float account) {
        this.account = account;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}