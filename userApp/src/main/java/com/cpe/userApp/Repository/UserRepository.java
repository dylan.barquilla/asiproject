package com.cpe.userApp.Repository;

import com.cpe.userApp.Model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserModel, Integer> {
    List<UserModel> findByLoginAndPwd(String login, String pwd);
}
