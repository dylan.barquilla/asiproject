package com.cpe.cardDto;

public class CardDto extends CardReferenceDto {
    private float energy;
    private float hp;
    private float defence;
    private float attack;
    private float price;

    //private UserModel user;
    private Integer userId;

    //private StoreModel store;
    private Integer storeId;

    public CardDto() {
        super();
    }

    public CardDto(CardReferenceDto cardRef) {
        super(cardRef);
    }

    public CardDto(String name, String description, String family, String affinity, float energy, float hp,
                   float defence, float attack, String imgUrl, String smallImg, float price) {
        super(name, description, family, affinity,imgUrl,smallImg);
        this.energy = energy;
        this.hp = hp;
        this.defence = defence;
        this.attack = attack;
        this.price=price;
    }
    public float getEnergy() {
        return energy;
    }
    public void setEnergy(float energy) {
        this.energy = energy;
    }
    public float getHp() {
        return hp;
    }
    public void setHp(float hp) {
        this.hp = hp;
    }
    public float getDefence() {
        return defence;
    }
    public void setDefence(float defence) {
        this.defence = defence;
    }
    public float getAttack() {
        return attack;
    }
    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUser(Integer userId) {
        this.userId = userId;
    }

    public void setStore(Integer storeId) {
        this.storeId=storeId;
    }

    public Integer getStore() {
        return storeId;
    }

    public float computePrice() {
        return this.hp * 20 + this.defence*20 + this.energy*20 + this.attack*20;
    }


}
