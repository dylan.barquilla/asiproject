package com.cpe.cardApp;

import com.cpe.common.Poney;
import org.apache.activemq.Message;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class BusListener {

    @JmsListener(destination = "RESULT_BUS_MNG")
    public void receiveMessageResult(Poney poney) {

        System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED Poney MSG=["+poney+"]");

    }

    @JmsListener(destination = "A")
    public void receiveMessageA(Poney poney, Message message) {

        System.out.println("[BUSLISTENER] [CHANNEL A] RECEIVED Poney MSG=["+poney+"]");

    }

    @JmsListener(destination = "B")
    public void receiveMessageB(Poney poney, Message message) {

        System.out.println("[BUSLISTENER] [CHANNEL B] RECEIVED Poney MSG=["+poney+"]");

    }
}
