package com.cpe.cardApp.Repository;

import com.cpe.cardApp.Model.CardModel;
import com.cpe.userDto.UserDto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUserId(Integer u);
}
