package com.cpe.cardApp.Repository;

import com.cpe.cardApp.Model.CardReference;
import org.springframework.data.repository.CrudRepository;

public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}
