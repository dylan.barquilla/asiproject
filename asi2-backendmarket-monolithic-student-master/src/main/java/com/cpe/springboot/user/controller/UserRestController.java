package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping("/users")
    private List<UserModel> getAllUsers() {
        return userService.getAllUsers();

    }

    @RequestMapping("/user/{id}")
    private UserModel getUser(@PathVariable String id) {
        Optional<UserModel> ruser;
        ruser = userService.getUser(id);
        if (ruser.isPresent()) {
            return ruser.get();
        }
        return null;

    }

    @RequestMapping("/userByLogin/{login}")
    private UserModel getUserByLogin(@PathVariable String login) {
        List<UserModel> ruser;
        ruser = userService.getUserByLogin(login);
        if (ruser.size() > 0) {
            return ruser.get(0);
        }
        return null;
    }



    @RequestMapping(method = RequestMethod.POST, value = "/user")
    public void addUser(@RequestBody UserModel user) {
        userService.addUser(user);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/user/{id}")
    public void updateUser(@RequestBody UserModel user, @PathVariable String id) {
        user.setId(Integer.valueOf(id));
        userService.updateUser(user);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/user/{id}")
    public void deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/auth")
    private UserModel getAllCourses(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
        List<UserModel> UserConnected = userService.getUserByLoginPwd(login, pwd);
        if (UserConnected.size() > 0) {
            return UserConnected.get(0);
        }
        return null;
    }


}
