var express = require('express');
var app = express();

server = app.listen(8080, function(){
    console.log('server is running on port 8080')
});



// Part socket

var socket = require('socket.io');
io = socket(server);



io.on('connection', (socket) => {
	
    console.log(socket.id);

    socket.on('SEND_MESSAGE', function(data){
		console.log("Message reçu");
		console.log(data);
		
        io.emit('RECEIVE_MESSAGE', data);
		
		console.log("Message envoyé");
    })
	
	
	socket.on('LOGIN_USER', function(data){
		console.log("LOG");
		
        io.emit('LOGIN_PASSED', "bonjour");
		
		console.log("LOG OK");
    })
	
	socket.on('GET_USERS', function(data){
		console.log("GET USERS");
		
        users = ["Toto", "Paul", "Serge", "Luc"];
		io.emit('RETURN_USERS', users);
		
		console.log("USERS Sent");
    })
	
	
	socket.on('JOIN_GAME', function(data){
		console.log("JOIN_GAME");
		console.log(data);

		let res = "test";
		
		io.emit('START_GAME', res);
		
		console.log("game Sent");
    })
	
});