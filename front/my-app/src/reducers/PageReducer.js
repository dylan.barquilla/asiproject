/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const pageReducer= (state={descriptionPage:"",selectedPage:[]},action) => {
    console.log("Page reducer");
        console.log(action);
        switch (action.type) {
            case 'MOVE_PAGE':
                let descriptionPage= action.newPage;
                if(action.newPage==="Sell" || action.newPage==="Buy"){
                    state.selectedPage = "ShopRoom";
                }else{
                    state.selectedPage=action.newPage;
                }
                //copy list because state is immutable
                let newPageCopy=JSON.parse(JSON.stringify(state.selectedPage));
            return {descriptionPage:descriptionPage,selectedPage:newPageCopy};

            case 'LOAD_PAGE':
            //return directly the list of the action that will besome the new state value
            return {descriptionPage:state.descriptionPage, selectedPage:action.newPage};

            default:
            return state;
        }
    }
    export default pageReducer;