/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const userReducer= (state={status:false,currentUser:[],listUser:[]},action) => {
    console.log("User reducer");
        console.log(action);
        switch (action.type) {
            case 'ADD_USER': 
                state.listUser.push(action.newUser);
                //copy list because state is immutable
                let newUserList=JSON.parse(JSON.stringify(state.listUser));
            return {status:state.status,listUser:newUserList, currentUser:state.currentUser};

            case 'REMOVE_USER': break;

            case 'CONNECT_USER':
            return {status:true, currentUser:action.userCurrent,listUser:state.listUser};

            case 'DISCONNECT_USER': break;

            case 'LOAD_USERS':
            //return directly the list of the action that will besome the new state value
            return {status:state.status, currentUser:state.currentUser, listUser:action.loadUser};

            case 'REFRESH_USER':
            //return directly the list of the action that will besome the new state value
            return {currentUser:action.refreshUser, status:state.status, listUser:state.listUser};            

            default:
            return state;
        }
    }
    export default userReducer;