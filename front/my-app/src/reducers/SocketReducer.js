/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const valueListReducer= (state={socketConnect:[]},action) => {
    console.log("reducer");
        console.log(action);
        switch (action.type) {
            case 'SOCKET_CONNECT': 
            return{socketConnect:action.socket};

            default:
            return state;
        }
    }
    export default valueListReducer;