/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const cardReducer= (state={listUserCard:[],currentCard:[],listCard:[]},action) => {
    console.log("Card reducer");
        console.log(action);
        switch (action.type) {
            case 'ADD_USER_CARD': 
                //Ajout de la carte à la liste utilisateur
                let newCardUser = state.listUserCard.push(action.newCard);  
                
                //Retrait de la carte à la liste globale 
                let newListCard=[];             
                for(let i=0; i<state.listCard.length;i++){
                    if(state.listCard[i].id!=action.newCard.id){
                        newListCard.push(state.listCard[i]);
                    }
                }
            return {listUserCard:newCardUser, currentCard:state.currentCard, listCard:newListCard};

            case 'SELECT_CARD': 
                let cardCurrent = action.cardCurrent;
                if(action.cardCurrent === undefined){
                    cardCurrent = "";
                }
            return {listUserCard:state.listUserCard, listCard:state.listCard, currentCard:cardCurrent};

            case 'REMOVE_USER_CARD':
                //Retrait de la carte à la liste utilisateur
                let newListCardUser=[];             
                for(let i=0; i<state.listUserCard.length;i++){
                    if(state.listUserCard[i].idCard!=action.deleteCard.id){
                        newListCardUser.push(state.listUserCard[i]);
                    }
                }
                
                //Ajout de la carte à la liste globale 
                let newCardList = state.listCard.push(action.deleteCard); 
            return {listUserCard:newListCardUser, currentCard:state.currentCard, listCard:newCardList};

            case 'LOAD_CARDS':
            //return directly the list of the action that will besome the new state value
            return {listUserCard:state.listUserCard, currentCard:state.currentCard, listCard:action.loadCard};

            case 'LOAD_USER_CARD':
                let userCardList=[];        
                if(action.currentUser !== undefined){     
                    for(let i=0; i<state.listUserCard.length;i++){
                        if(state.listUserCard[i].userId==action.currentUser.id){
                            userCardList.push(state.listUserCard[i]);
                        }
                    }
                }else{
                    userCardList = action.loadUserCard;
                }
            return {listUserCard:userCardList, currentCard:state.currentCard, listCard:state.listCard};

            default:
            return state;
        }
    }
    export default cardReducer;