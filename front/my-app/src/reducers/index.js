import { combineReducers } from 'redux';
import pageReducer from './PageReducer';
import userReducer from './UserReducer';
import cardReducer from './CardReducer';
import socketReducer from './SocketReducer';

/*
reducer that can contains set of reducer, usefull when several reducers are used at a timeS
*/
const globalReducer = combineReducers({
    pageReducer: pageReducer,
    userReducer: userReducer,
    cardReducer: cardReducer,
    socketReducer : socketReducer
});
export default globalReducer;