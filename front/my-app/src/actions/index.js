/*
List of action use to interact with the store via the reducers
*/

//reducers about page
export const movePage=(newPage)=>{

    return {type:'MOVE_PAGE',newPage:newPage};
};

export const loadPage=(newPage)=>{

    return {type:'LOAD_PAGE',newPage:newPage};
};

//reducers about user
export const addUser=(newUser)=>{

    return {type:'ADD_USER',newUser:newUser};
};

export const removeUser=(deleteUser)=>{

    return {type:'REMOVE_USER',deleteUser:deleteUser};
};

export const connectUser=(connectUser)=>{

    return {type:'CONNECT_USER', userCurrent:connectUser};
};

export const disconnectUser=(disconnectUser)=>{

    return {type:'DISCONNECT_USER',disconnectUser:disconnectUser};
};

export const loadUser=(loadUser)=>{

    return {type:'LOAD_USERS',loadUser:loadUser};
};

export const refreshUser=(refreshUser)=>{

    return {type:'REFRESH_USER',refreshUser:refreshUser};
};

//reducers about card

export const addUserCard=(newCard)=>{

    return {type:'ADD_USER_CARD',newCard:newCard};
};

export const removeUserCard=(deleteCard)=>{

    return {type:'REMOVE_USER_CARD',deleteCard:deleteCard};
};

export const loadCard=(loadCard)=>{

    return {type:'LOAD_CARDS',loadCard:loadCard};
};

export const selectCard=(selectCard)=>{

    return {type:'SELECT_CARD', cardCurrent:selectCard};
};

export const loadUserCard=(loadUserCard)=>{

    return {type:'LOAD_USER_CARD',loadUserCard:loadUserCard,currentUser:undefined};
};

//reducers about socket
export const socketConnected=(socket)=>{

    return {type:'SOCKET_CONNECT', socket:socket};
};

// export const loadUserCard=(loadUserCard)=>{

//     return {type:'LOAD_USER_CARD',loadUserCard:loadUserCard,currentUser:undefined};
// };