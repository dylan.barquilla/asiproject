import React from "react";
import { connect } from 'react-redux';
import io from "socket.io-client";
import Room from "./Room";

class Chat extends React.Component{


    constructor(props){
        super(props);

        this.state = {
            username: '',
            recipient: '',
            message: '',
            messages: [],
            listUsers: [],
            socket: '',
            IsPlayed: false
        };

        this.socket = this.props.socket;


        this.sendMessage=this.sendMessage.bind(this);
        this.addMessage=this.addMessage.bind(this);
        this.setListUsers=this.setListUsers.bind(this);
        this.startGame=this.startGame.bind(this);


        // When we receive a message
        this.socket.on('NEW_MESSAGE', this.addMessage);


        // When we recieve the list of all users
        this.socket.on('RETURN_USERS', this.setListUsers);
        
        // We say that we want to receiv the list of users
        this.socket.emit('GET_USERS'); 
    }

    // REGION methods //

    // We set the list of users
    setListUsers(data) {
        this.setState({listUsers: data});
        if (this.state.listUsers.length !== 0) {
            this.setState({recipient: this.state.listUsers[0]});
        }
    }


    // We send a message
    sendMessage(ev) {
        ev.preventDefault();
        console.log("this.state");
        console.log(this.state);
        this.socket.emit('SEND_MESSAGE', {
            recipient: this.state.recipient,
            message: this.state.message,
            username: this.props.username
        });

        this.setState({message: ''});
    }


    // We add a message
    addMessage(data) {
        console.log(data);
        this.setState({messages: [...this.state.messages, data.username + ": " + data.message]});
    };


    // Click on the start button
    startGame() {
        this.setState({IsPlayed: true});
    }

    render(){
        let res = "";

        if (!this.state.IsPlayed){
            res = (
            <div className="container">
                <div className="row">
                    <div className="col-4">
                        <div className="card">
                            <div className="card-body">
                                <div className="card-title">Chat of user: {this.props.username}</div>
                                <hr/>
                                
                                <div className="footer">

                                
                                    <select value={this.state.recipient} 
                                            onChange={(e) => this.setState({recipient: e.target.value})}>
                                        {this.state.listUsers.map((user) => <option key={user} value={user}>{user}</option>)}
                                    </select>

                                    <br/>

                                    <input type="text" placeholder="Message" className="form-control" value={this.state.message} onChange={ev => this.setState({message: ev.target.value})}/>
                                   
                                    <br/>

                                    <button onClick={this.sendMessage} className="btn btn-primary form-control">
                                        Send
                                    </button>


                                    <button onClick={this.startGame} className="btn btn-primary form-control">
                                        PLAY
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-6">
                        <div className="messages">
                            {this.state.messages.map(message => {
                                return (
                                    <div>{message}</div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
            )
        }
        else 
        {
            res = (
                 <Room username={this.props.username}
                  socket={this.props.socket}>

                </Room>
            )
        }
        


        return res;
    }
}

export default connect()(Chat);