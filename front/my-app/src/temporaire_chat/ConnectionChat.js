import React from 'react';
import { connect } from 'react-redux';
import Form from '../commun/Form';
import Button from '../commun/Button';
import Login from './Login';
import Error from './Error';
import UserForm from '../userForm/UserForm';
import '../../css/connection.css';
import io from "socket.io-client";
import Chat from './Chat';


 class ConnectionChat extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            Login : '',
            Pwd : '',
            IsConnected: false,
            Trigger: '',
            ErrorMessage: ''
        };


        this.connection=this.connection.bind(this);
        this.onLoginPassed=this.onLoginPassed.bind(this);
        this.onSocketDisconnected=this.onSocketDisconnected.bind(this);

        this.socket = io('localhost:3000');
        this.socket.on('LOGIN_PASSED', this.onLoginPassed);
        
        this.socket.on('disconnect', this.onSocketDisconnected);
        

    }


    /**
     * Call function when connection form is subbmited
     * @param {*} ev 
     */
    connection(ev) 
    {
        ev.preventDefault();

        let evTarget = ev.currentTarget;
        
        let surname = evTarget[0].value;
        let pass = evTarget[1].value;


        console.log("ev");
        console.log(ev);
        this.socket.emit('LOGIN_USER', {
            login: surname,
            pwd: pass,
        });

               
    }

    /**
     * When we receive the call LOGIN PASSED from the socket
     * @param {*} data the username or null
     */
    onLoginPassed(data) {
        if (data === null || data === undefined) {

            this.setState({ErrorMessage: "Bad login, please try with another usernamne/password."});

        } 
        
        else {
            console.log("REUSSITE");

            this.setState({IsConnected: true, Login: data});
            this.setState({Trigger: this.trigger});
        }
    }

    /**
     * Callback function when socket is not connected
     * @param {*} _data 
     */
    onSocketDisconnected(_data) {
        console.log("DISCONNECTED");
        this.setState({ErrorMessage: "We cannot connect to the socket from backend."});
    }
    


    render() {
        let res = "";
        
        if (!this.state.IsConnected) {
            res = (<div class="ui placeholder segment">
                        <div class="ui two column very relaxed stackable grid">
                            <div class="column subcontainer" id="test">
                                <Login trigger={this.connection}>
                                    
                                </Login>
                            </div>

                        </div>
                    </div> ) ;
        } 
        
        else {
            res = 
            (
                <Chat socket={this.socket}
                      username={this.state.Login}>

                </Chat>    
            );
        }


        if (this.state.ErrorMessage) {
            res += (
                <Error Message={this.state.ErrorMessage}>

                </Error>
            )
        }



        return res;
    }



}
export default connect()(ConnectionChat);