import React, { Component } from 'react';
import { connect } from 'react-redux';

class Test extends Component {
    constructor(props){
        super(props);
    }
    render() {
        console.log("Test");
        console.log(this.props.current_page);
        return(
            <div>
                Test réussi
            </div>
        );
    }
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        current_page: state.pageReducer.selectedPage
    } };

export default connect(mapStateToProps) (Test);