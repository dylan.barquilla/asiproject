import React from 'react';
import { connect } from 'react-redux';

import MenuButtonDisplay from '../components/MenuButtonDisplay';

import '../../../css/home.css';
import { selectCard } from '../../../actions';

 class Home extends React.Component{     
     
    constructor(props) {
        super(props);
        this.state = {
        };
    }
        render() {
            let currentCard = [];            
            this.props.dispatch(selectCard(currentCard)); 
            return(
                <div>
                    <div className="wrapper">
                        <div></div>
                        <div>
                            <MenuButtonDisplay description="Buy">
                            </MenuButtonDisplay>
                        </div>
                        <div></div>
                        <div>
                            <MenuButtonDisplay description="Sell">
                            </MenuButtonDisplay>
                        </div>
                        <div></div>
                    </div>

                    <div className="wrapper">
                        <div></div>
                        <div></div>
                        <div>
                            <MenuButtonDisplay description="Play">
                            </MenuButtonDisplay>
                        </div>
                        <div></div>
                        <div></div>
                    </div>
                </div>                
            );           
          }

    }
      
    export default connect()(Home)