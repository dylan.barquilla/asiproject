import React from 'react';
import {movePage} from '../../../actions'
import { connect } from 'react-redux';

 class HomeButton extends React.Component{     
     
    constructor(props) {
        super(props);
        this.state = {
        };        
    }

    handleClick(event) {
        const value = event.target.value ;
        this.props.dispatch(movePage(value));   
    }
    
        render() {
            return(
                <button className="massive ui button" name="movePage" value={this.props.description} onClick={(ev)=>{this.handleClick(ev)}}>
                    {this.props.description}
                </button>
            );
        }
    }
      
    export default connect() (HomeButton);
