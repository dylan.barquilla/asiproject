import React, { Component } from 'react';
import { connect } from 'react-redux';

//import components
import Header from '../../header/containers/Header'
import Connection from '../../connection/containers/Connection'
import Home from '../../home/containers/Home'
import ShopRoom from '../../shopRoom/containers/ShopRoom';
import Chat from '../../Chat/Chat';

class PageSelection extends Component {
    constructor(props){
        super(props);
    }
    render() {
        let res = "";
        let header =    <Header >
                        </Header>;
        switch (this.props.current_page) {
            case "Connection":
            res = (
                <div>
                    <div>
                        {header}
                    </div>
                    <div>
                        <Connection>                    
                        </Connection>
                    </div>
                </div>
            )
            break;

            case "Play":
            res = (
                <div>
                    <div>
                        {header}
                    </div>
                    <div>
                        <Chat>                    
                        </Chat>
                    </div>
                </div>
            )
            break;
            
            case "Home":
            res = (
                <div>
                    <div>
                        {header}
                    </div>
                    <div>
                        <Home>                    
                        </Home>
                    </div>
                </div>
            )
            break;

            case "ShopRoom":
            res = (
                <div>
                    <div>
                        {header}
                    </div>
                    <div>
                        <ShopRoom>                    
                        </ShopRoom>
                    </div>
                </div>
            )
            break;

            default: 
                res = (
                    <div>
                        <div>
                        {header}
                        </div>
                        <div>
                        This page does not exist.
                        </div>
                    </div>
                )
        }   
        return res;   
    
    }  
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        current_page: state.pageReducer.selectedPage
    } };

export default connect(mapStateToProps) (PageSelection);
