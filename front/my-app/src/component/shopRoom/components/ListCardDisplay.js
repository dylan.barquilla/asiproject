import React from 'react';
import { connect } from 'react-redux';
import { selectCard } from '../../../actions';


 class ListCardDisplay extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    handleClick(event){
        console.log("ListCardDisplay");
        if(this.props.listToPrint!== undefined){
            let cardSelect = this.props.listToPrint;
            for(let i=0;i<cardSelect.length;i++){
                if(cardSelect[i].id == event.currentTarget.id){
                    console.log("ok");
                    this.props.dispatch(selectCard(cardSelect[i]));
                }
            }
        }
    }

        render() {
            let array_value=[];
            if(this.props.listToPrint!==undefined){                
                array_value=this.props.listToPrint;
            }
            let list = array_value.map(item =>
                <tr id={item.id} onClick={(ev)=>this.handleClick(ev)}>
                    <td>
                        {item.name}
                    </td>
                    <td>
                        {item.description}
                    </td>
                    <td>
                        {item.family}
                    </td>
                    <td>
                        {item.affinity}
                    </td>
                    <td>
                        {item.energy}
                    </td>
                    <td>
                        {item.hp}
                    </td>
                    <td>
                        {item.price} €
                    </td>
                </tr>
            );  
            return( 
                <table className="ui selectable celled table">
                    <thead>
                        <tr>                                
                            <th>
                                Name
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Family
                            </th>
                            <th>
                                Affinity
                            </th>
                            <th>
                                Energy
                            </th>
                            <th>
                                HP
                            </th>
                            <th>
                                Price
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {list}
                    </tbody>
                </table>
            );            
        }
    }
    export default connect() (ListCardDisplay);
