import React from 'react';
import { connect } from 'react-redux';

import {addUserCard,connectUser,removeUserCard} from '../../../actions'


 class CompleteCardDisplay extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            data : {
                user_id : "",
                card_id : ""
            },
            cardCurrent :[]
        };          

        //Gestion des sockets
        this.onLoginPassed=this.onLoginPassed.bind(this); 
        this.props.socket.removeAllListeners('LOGIN_PASSED');
        this.props.socket.on('LOGIN_PASSED', this.onLoginPassed);    
        this.handleClick=this.handleClick.bind(this);

    }

    handleClick(event){
        let login = this.props.currentUser.login;
        let pwd = this.props.currentUser.pwd;
        let socket = this.props.socket;
        this.state.data.user_id = this.props.currentUser.id;
        this.state.data.card_id = this.props.currentCard.id;
        var request = new XMLHttpRequest();
        console.log(this.state.data);
        request.open('POST', 'http://localhost:8083/sell', true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.send(JSON.stringify(this.state.data))
        request.onreadystatechange = function() { // Call a function when the state changes.
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
               console.log(request);
               if(request.responseText==="true"){
                    //en attendant la mise en place du removeUserCard
                    socket.emit('LOGIN_USER', {
                        login: login,
                        pwd: pwd
                    });
                   //this.props.dispatch(removeUserCard(this.state.cardCurrent)); 
               }
            }
        }
        // if(this.props.descriptionButton === "BUY")
        // {
        //     //Vérification des ressources de l'acheteur
        //     if(this.props.currentUser.money < this.props.currentCard.price){
        //         alert("You don't have enough money to buy this card, sorry =(");
        //     }else{
        //         //Si ressources suffisantes, ajout de la carte et MAJ des données utilisateur
        //         this.state.userCurrent.money = this.props.currentUser.money - this.props.currentCard.price;
        //         this.props.dispatch(addUserCard(this.props.currentCard)); 
        //         this.props.dispatch(refreshUser(this.state.userCurrent));
        //     }
        // }else{
        //     //Vente d'une carte, retrait de la carte et MAJ des données utilisateur
        //     this.state.userCurrent.money = this.props.currentUser.money + this.props.currentCard.price;
        //     this.props.dispatch(removeUserCard(this.props.currentCard)); 
        //     this.props.dispatch(refreshUser(this.props.currentUser));
        // }
    }
    

    


    /**
     * When we receive the call LOGIN PASSED from the socket
     * @param {*} data the username or null
     */
    onLoginPassed(value) {
        if (value === null || value === undefined) {

            alert("Bad login, please try with another usernamne/password.");

        } else {
            console.log("REUSSITE");

            this.props.dispatch(connectUser(value)); 
        }
        
        console.log("après envoi");
        console.log(value);
    }

        render() {
            let CardCurrent = this.props.currentCard;
            console.log(CardCurrent);
            this.state.cardCurrent = CardCurrent;
            console.log(this.state.cardCurrent);
            if(CardCurrent === undefined){
                return(<div></div>);
            }
            return (
                <div>
                    <div className="ui card">
                        <div className="content ui grid">
                            <div className="three column row">
                                <div className="column floated meta">{CardCurrent.energy}</div>                        
                                <div className="column">{CardCurrent.family}</div> 
                                <div className="column floated meta">{CardCurrent.hp}</div>  
                            </div>                      
                        </div>
                        <div className="ui small centered image">
                            <img src={CardCurrent.imgCard}/>{CardCurrent.name}
                        </div>
                        <div className="content">
                            <div className="description">
                            {CardCurrent.description}
                            </div>
                        </div>     
                    <button className="ui green massive button" onClick={(ev)=>this.handleClick(ev)}>
                        {this.props.descriptionButton}
                    </button>               
                    </div>
                </div>
            );
          }

    }
    export default connect() (CompleteCardDisplay);
