import React from 'react';

import { connect } from 'react-redux';

import ListCardDisplay from '../components/ListCardDisplay'
import CompleteCardDisplay from '../components/CompleteCardDisplay'

 class ShopRoom extends React.Component{     
     
    constructor(props) {
        super(props);
        this.state = {
            listToPrint : [],
        };
    }
        render() {
            
            //gestion vente card user
            if(this.props.descriptionPage === "Sell"){
                this.state.listToPrint = this.props.current_user.cardList;
            }
            if(this.props.descriptionPage === "Buy"){
            }

            return(
                <div className="ui grid center aligned page">
                    <div className="ten wide column">
                        <ListCardDisplay    descriptionButton={this.props.descriptionPage}
                                            listToPrint={this.state.listToPrint}
                                            currentUser={this.props.current_user}>
                        </ListCardDisplay>
                    </div>
                    <div className="six wide column"> 
                        <CompleteCardDisplay descriptionButton={this.props.descriptionPage}
                                            currentCard={this.props.current_card}
                                            listToPrint={this.state.listToPrint}
                                            currentUser={this.props.current_user}
                                            socket={this.props.socket}>
                        </CompleteCardDisplay>
                    </div>
                </div>                
            );           
          }

    }
    
//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.currentUser,
        current_card : state.cardReducer.currentCard,
        list_card : state.cardReducer.listCard,
        list_user_card : state.cardReducer.listUserCard,
        descriptionPage : state.pageReducer.descriptionPage,
        socket : state.socketReducer.socketConnect
    } 
};

export default connect(mapStateToProps) (ShopRoom);
