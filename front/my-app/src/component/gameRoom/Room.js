import React from "react";
import { connect } from 'react-redux';

class Room extends React.Component{


    constructor(props){
        super(props);

        this.state = {
            username: '',
            opponent: '',
            currentUser: '',
            IsInGame: false
        };


        this.startGame=this.startGame.bind(this);


        // When we receive a message
        this.props.socket.on('START_GAME', this.startGame);
        
        // We say that we want to receiv the list of users
        this.props.socket.emit('JOIN_GAME', {username: this.props.username}); 
    }

    // REGION methods //

    // Start a game
    startGame(data) {
        
        this.setState({currentUser: data.User1,
                        opponent: data.User2,
                        IsInGame: true
                    });
    }



    render(){
        let res = "";

        if (!this.state.IsInGame) {
            res = (
                <div>
                    En attente de partie...
                </div>
            )
        }
        else {
            res = (
                <div>
                    Je vais jouer avec : {this.state.opponent.login}
                </div>
            );
        }


        return res;
    }
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        socket: state.socketReducer.socketConnect
    } };

export default connect(mapStateToProps)(Room);
