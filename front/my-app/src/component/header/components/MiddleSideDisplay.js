import React from 'react';

import { connect } from 'react-redux';

import {movePage} from '../../../actions'

class MiddleSideDisplay extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            page:"no"
        };        
    }

    handleClick(event){      
        if(this.state.page!== "no"){
        this.props.dispatch(movePage(this.state.page)); 
        }
    }

    render() {
        if(this.props.display==="yes"){
        this.state.page = "Home";
        }
        console.log("MIDDLE");
        console.log(this.props.display);
        console.log(this.state.page);
            return(  
                <a className="item" onClick={(ev)=>{this.handleClick(ev)}}>
                    POKEMON-APP (home)
                </a>
            );
        }
    }
export default connect() (MiddleSideDisplay);