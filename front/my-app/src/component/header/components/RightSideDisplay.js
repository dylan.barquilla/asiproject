import React from 'react';
import { connect } from 'react-redux';
import { disconnectUser, movePage } from '../../../actions';

class RightSideDisplay extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            img:"",
            login:"Please Connect",
            button_connect :""
        };
    }

    handleClick(event){
        console.log("disconnect");
        this.props.dispatch(disconnectUser(this.props.current_user));
        console.log("disconnect 2");
        console.log(this.props.current_user);
        this.props.dispatch(movePage("Connection"));
    }

    render() {
        if(this.props.display=="yes"){
            //this.state.img = <div><img src={this.props.current_user.img}> </img></div>;
            this.state.login = this.props.current_user.login;
            this.state.button_connect =  
            <button className="circular ui icon red button" onClick={(ev)=>this.handleClick(ev)}>
                <i className="icon power off" ></i>
            </button>;
        }
            return(  
                    <div className="ui grid center aligned page">
                        <div className="twelve wide column">
                        {this.state.img}
                        {this.state.login}  
                        </div>
                        <div className="four wide column">
                            {this.state.button_connect}
                        </div> 
                    </div>     
            );
        }
    }
export default connect() (RightSideDisplay);