import React from 'react';

class LeftSideDisplay extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            money:"€€"
        };
    }

    render() {
            if(this.props.display==="yes"){
            this.state.money = this.props.current_user.money;
        }
            return(  
                <div >
                    {this.state.money}€         
                </div>
            );
        }
    }
export default (LeftSideDisplay);