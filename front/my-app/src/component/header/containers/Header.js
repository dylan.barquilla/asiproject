import React from 'react';
import { connect } from 'react-redux';

import LeftSideDisplay from '../components/LeftSideDisplay';
import MiddleSideDisplay from '../components/MiddleSideDisplay';
import RightSideDisplay from '../components/RightSideDisplay';

class Header extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            display : "no"
        };
    }

    render() {
        if(this.props.logged === false|| this.props.current_user === undefined){ 
            this.state.display ="no";
        }else{
            this.state.display ="yes";
        }
        console.log("HEADER");
        console.log(this.props.current_user);
        console.log(this.props.listUser);
        console.log(this.state.display);
        return(  
            <div className="ui three item menu">
                <div className="item">
                    <LeftSideDisplay display={this.state.display}
                                        current_user={this.props.current_user}></LeftSideDisplay>
                </div>
                <div className="item">
                    <MiddleSideDisplay display={this.state.display}
                                        current_user={this.props.current_user}></MiddleSideDisplay>
                </div>
                <div className="item">
                    <RightSideDisplay display={this.state.display}
                                        current_user={this.props.current_user}></RightSideDisplay>
                </div>
            </div>
        );
        }
    }

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.currentUser,
        listUser: state.userReducer.listUser,
        logged: state.userReducer.status
    } 
};
      
export default connect(mapStateToProps)(Header);
