import React from 'react';
import { connect } from 'react-redux';

import '../../../css/form.css';
import '../../../css/connection.css';

import {addUser} from '../../../actions'


 class RegistrationForm extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            name :'',
            surname : '',
            password :'',
            login : '',
            img :'',
            money : ''
        };        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }    

    handleChange(event) {
        switch(event.target.name){
            case "name" :
                this.setState({name: event.target.value});
            break;
            case "surname" :
                this.setState({surname: event.target.value});
            break;
            case "login" :
                this.setState({login: event.target.value});
            break;
            case "password" :
                this.setState({password: event.target.value});
            break;
            case "money" :
                this.setState({money: event.target.value});
            break;
            case "img" :
                this.setState({img: event.target.value});
            break;
            default: ;
        };
    }

    handleSubmit(event) {   
        let error = false;

        if(this.state.password==="" || this.state.login==="" || this.state.name==="" || this.state.surname===""){
            error=true;                
            alert("Please complete all required fields");
        }

        if(error===false){            
            this.props.dispatch(addUser(this.state)); 
        }
    }

        render() {
            return(
                <div className="container">
                    <h4 className="ui dividing header">Sign Up</h4>       
                        <div className="field">
                            <label>Name *</label>
                            <div className="field">
                                <input type="text" name="name" value={this.state.name} placeholder="Enter your name" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div>
                            <label>Surname *</label>
                            <div className="field">
                                <input type="text" name="surname" value={this.state.surname} placeholder="Enter your surname" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div>
                            <label>Login *</label>
                            <div className="field">
                                <input type="text" name="login" value={this.state.login} placeholder="Enter your login" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div>
                            <label>Password *</label>
                            <div className="field">
                                <input type="password" name="password" value={this.state.password} placeholder="Enter your password" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div>
                            <label>Money</label>
                            <div className="field">
                                <input type="number" name="money" value={this.state.money} placeholder="Enter your money" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div>
                            <label>Avatar</label>
                            <div className="field">
                                <input type="img" name="img" value={this.state.img} placeholder="Enter your avatar" onChange={(ev)=>this.handleChange(ev)}></input>
                            </div> 
                            <input type="submit" value="Subscribe" onClick={(ev)=>this.handleSubmit(ev)}></input>
                        </div>
                </div>
            );
          }
    }
    
export default connect() (RegistrationForm);
