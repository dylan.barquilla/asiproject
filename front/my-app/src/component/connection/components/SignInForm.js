import React from 'react';

import '../../../css/form.css';
import '../../../css/connection.css';

import { connect } from 'react-redux';

import {connectUser, movePage, loadUserCard} from '../../../actions'

class LoginForm extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
            login : '',
            password :''
        };
        
        //Gestion des sockets
        this.onLoginPassed=this.onLoginPassed.bind(this);        
        this.props.socket.on('LOGIN_PASSED', this.onLoginPassed);       
    }

    processInput(event){
        const target = event.target.value;
        const name = event.target.name;
            this.setState({[name]:target});
    }
    
    submitOrder(data){        
        
        console.log("avant envoi");
        console.log(this.state);
        this.props.socket.emit('LOGIN_USER', {
            login: this.state.login,
            pwd: this.state.password,
        });
    }

    
    /**
     * When we receive the call LOGIN PASSED from the socket
     * @param {*} data the username or null
     */
    onLoginPassed(data) {
        if (data === null || data === undefined) {

            alert("Bad login, please try with another usernamne/password.");

        } else {
            console.log("REUSSITE");

            this.props.dispatch(connectUser(data));  
            this.props.dispatch(movePage("Home"));
        }
        
        console.log("après envoi");
        console.log(data);
    }

    render() {        
        return(
            <div className="container">
                <h4 className="ui dividing header">Sign in</h4>                                                    
                    <div className="field">
                        <label>Login</label>
                        <div className="field">
                            <input type="text" name="login" value={this.state.login} placeholder="Enter your login" onChange={(ev)=>{this.processInput(ev)}}/>
                        </div>
                        <label>Password</label>
                        <div className="field">
                            <input type="password" name="password" value={this.state.password} placeholder="Enter your password"  onChange={(ev)=>{this.processInput(ev)}}></input>
                        </div>
                        <input type="submit" value="Sign in" onClick={(data)=>this.submitOrder(data)}></input>
                    </div>
            </div>
        );
    }
}
export default connect() (LoginForm);
