import React from 'react';
import { connect } from 'react-redux';

import LoginForm from '../components/SignInForm';

import '../../../css/connection.css';
import RegisterForm from '../components/RegisterForm';

 class Connection extends React.Component{
     
    constructor(props) {
        super(props);
        this.state = {
        };
    }

        render() {
            return(
                <div className="ui placeholder segment">
                    <div className="ui two column very relaxed stackable grid">
                        <div className="column subcontainer">
                            <LoginForm listUser={this.props.listUser}
                                        socket={this.props.socket}>                                
                            </LoginForm>
                        </div>

                        <div className="middle aligned column subcontainer">
                            <RegisterForm  listUser={this.props.listUser}
                                            socket={this.props.socket}>                                  
                            </RegisterForm>
                        </div>
                    </div>
                    <div className="ui vertical divider ">
                        Or
                    </div>
                </div>           
            );
        }
    }

    //link data from the store to local props
    const mapStateToProps = (state, ownProps) => {
        return {
            listUser: state.userReducer.listUser,
            socket : state.socketReducer.socketConnect
        } };
    
    export default connect(mapStateToProps) (Connection);