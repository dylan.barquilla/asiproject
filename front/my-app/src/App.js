import React, { Component } from 'react';

import io from 'socket.io-client';

import './lib/Semantic-UI-CSS-master/semantic.min.css'; 

import * as jsonSource from './sources/donnees';

//import components
import PageSelection from './component/pageSelection/containers/PageSelection'

//import needed to use redux with react.js
import { createStore } from 'redux';
import globalReducer from './reducers/index'
import { Provider } from 'react-redux';

//import action to load the first time the list of values
import {loadPage,loadUser,loadCard,loadUserCard,socketConnected} from './actions'
import Connection from './component/connection/containers/Connection';

//create a store and associate reducers to it
const store = createStore(globalReducer);

class App extends Component {
  constructor(props){
    super(props);

    //call back-end    
    let socket = io('localhost:3000'); 
    store.dispatch(socketConnected(socket));

    //call the action loadPage to load initial page
    let current_page="Connection"
    store.dispatch(loadPage(current_page));

  //   //call the action loadUser to load initial user list
  //   let userList=jsonSource.default.users;
  //   store.dispatch(loadUser(userList));
    
  //   //call the action loadUser to load initial user list
  //   let cardList=jsonSource.default.cards;
  //   store.dispatch(loadCard(cardList));

  //   //call the action loadUser to load initial user list
  //   let cardUserList=jsonSource.default.userCards;
  //   store.dispatch(loadUserCard(cardUserList));
  }
  render() {
    return (
      <Provider store={store} >
        <div>
          <PageSelection></PageSelection>
        </div>
      </Provider>
    );
  }
}

export default App;
