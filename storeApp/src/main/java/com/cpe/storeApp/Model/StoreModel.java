package com.cpe.storeApp.Model;

import com.cpe.cardDto.CardDto;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class StoreModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL,
            mappedBy = "store")
    private Set<CardDto> cardList = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CardDto> getCardList() {
		return cardList;
	}

	public void setCardList(Set<CardDto> cardList) {
		this.cardList = cardList;
	}

	public void addCard(CardDto card) {
		card.setStore(this.id);
		this.cardList.add(card);
	}
	

}
