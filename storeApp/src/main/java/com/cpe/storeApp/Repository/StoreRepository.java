package com.cpe.storeApp.Repository;

import com.cpe.storeApp.Model.StoreModel;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<StoreModel, Integer> {


}
