package com.cpe.storeApp.Service;

import com.cpe.cardDto.CardDto;
import com.cpe.storeApp.Model.StoreModel;
import com.cpe.storeApp.Repository.StoreRepository;
import com.cpe.userDto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class StoreService {

    /*
    @Autowired
    CardModelService cardService;

    @Autowired
    UserService userService;
    */

    @Autowired
    StoreRepository storeRepository;

    private StoreModel store;

    public void generateNewStore(String name, int nb) {
        StoreModel store = new StoreModel();
        store.setName(name);

        /*
        List<CardDto> cardList = cardService.getRandCard(nb);
        for (CardDto c : cardList) {
            store.addCard(c);
        }
        */
        storeRepository.save(store);
        this.store = store;
    }

    public boolean buyCard(Integer user_id, Integer card_id) {

        /*
        Optional<UserDto> u_option = userService.getUser(user_id);
        Optional<CardDto> c_option = cardService.getCard(card_id);
        if (!u_option.isPresent() || !c_option.isPresent()) {
            return false;
        }
        UserDto u = u_option.get();
        CardDto c = c_option.get();
        if (u.getAccount() > c.getPrice()) {
            u.addCard(c.getId());
            u.setAccount(u.getAccount() - c.getPrice());
            userService.updateUser(u);
            return true;
        } else {
            return false;
        }

         */

        return false;
    }

    public boolean sellCard(Integer user_id, Integer card_id) {
        /*
        Optional<UserDto> u_option = userService.getUser(user_id);
        Optional<CardDto> c_option = cardService.getCard(card_id);
        if (!u_option.isPresent() || !c_option.isPresent()) {
            return false;
        }
        UserDto u = u_option.get();
        CardDto c = c_option.get();

        //c.setStore(this.store);
        c.setUser(null);
        cardService.updateCard(c);
        u.setAccount(u.getAccount() + c.computePrice());
        userService.updateUser(u);

         */
        return true;
    }

    public Set<CardDto> getAllStoreCard() {
        return this.store.getCardList();
    }
}
