package com.cpe.storeDto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class StoreDto implements Serializable {

    private static final long serialVersionUID = 5441589241029460660L;

    private Integer id;
    private String name;

    private Set<Integer> cardIdList = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Integer> getCardIdList() {
        return cardIdList;
    }

    public void setCardList(Set<Integer> cardIdList) {
        this.cardIdList = cardIdList;
    }

    public void addCardId(Integer cardId) {
        //card.setStore(this);
        this.cardIdList.add(cardId);
    }
}
