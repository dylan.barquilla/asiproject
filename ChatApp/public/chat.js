var socket;
var userId = null;
var destinataireId = null;

$(function(){

	socket = io.connect('http://localhost:3000')

	socket.emit('get_users', {});

	var message = $("#message");
	var username = $("#username");
	var send_message = $("#send_message");
	var send_username = $("#send_username");
	var login = $("#login_button");
	var chatroom = $("#chatroom");
	var messages = $("#messages");
	var feedback = $("#feedback");
	var startGame = $("#startgame_button");

	send_message.click(function(){
		var destinataireSelect = document.getElementById("destinataire");
		destinataireId = destinataireSelect.options[destinataireSelect.selectedIndex].value;
		console.log(destinataireSelect);
		socket.emit('NEW_MESSAGE', {message : message.val(), destinataire : destinataireId});
	})

	socket.on("NEW_MESSAGE", (data) => {
	    if (data.destinataire == userId || data.username == userId){
            feedback.html('');
            message.val('');
            messages.append("<p class='message'>" + data.username + ": " + data.message + "</p>")
	    }
	})

	startGame.click(function(){
		socket.emit('JOIN_GAME', {username : "user1"})
	})

	socket.on("LOGIN_PASSED", (data) => {
		console.log(data);
		
	})

	send_username.click(function(){
		console.log('cc');
		//socket.emit('change_username', {username : username.val()})
	})

	login.click(function(){
		var login = document.getElementById("login").value;
		var pwd = document.getElementById("pwd").value;
		socket.emit('LOGIN_USER', {login : login, pwd : pwd})
	})

	message.bind("keypress", () => {
		socket.emit('typing')
	})

	//Listen on typing
	socket.on('typing', (data) => {
	if (data.destinataire == userId){
	    feedback.html("<p><i>" + data.username + " est en train d'écrire..." + "</i></p>")
	}
	})

	socket.on('return_users', (data) => {
		var userSelect = document.getElementById("username");
		var destinataireSelect = document.getElementById("destinataire");
		JSON.parse(data).forEach(user => {
			var optionUsers = document.createElement("option");
			var optionDestinataires = document.createElement("option");
			optionUsers.text = user.login;
			optionUsers.value = user.login;

			optionDestinataires.text = user.login;
            optionDestinataires.value = user.login;

			userSelect.add(optionUsers);
			destinataireSelect.add(optionDestinataires);
		});
	})

});

function changeUser() {
	var userSelect = document.getElementById("username");
	userId = userSelect.options[userSelect.selectedIndex].value;
	socket.emit('change_username', {username : userId})
}

function changeDestinataire() {
	var destinataireSelect = document.getElementById("destinataire");
	destinataireId = destinataireSelect.options[destinataireSelect.selectedIndex].value;
	socket.emit('change_destinataire', {destinataire : destinataireId})

    var messagesToDelete = document.getElementsByClassName('message');
    for (var i = messagesToDelete.length-1; i >=0; i--){
    messagesToDelete[i].remove();
    }
}

