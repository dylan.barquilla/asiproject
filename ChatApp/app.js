const express = require('express')
const http = require('http');
//var game = require('./game');
var fs = require('fs');

// file is included here:
eval(fs.readFileSync('game.js')+'');
const app = express()
var userConnected = [];
var waitingUser = null;
var waitingUser2 = null;


//ejs template engine
app.set('view engine', 'ejs')

//middlewares
app.use(express.static('public'))

//routes
app.get('/', (req, res) => {
	res.render('index')
})

//Listen on port 3000
server = app.listen(3000)

//socket.io instantiation
const io = require("socket.io")(server)

//listen on every connection
io.on('connection', (socket) => {
    console.log('New user connected')

	//default username
	socket.username = "Anonymous"
	socket.destinataire = null;

    socket.on('GET_USERS', (data) => {
        http.get('http://localhost:8083/users', (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                var users = JSON.parse(data);
                let usersList = [];
                users.forEach(user => {
                    usersList.push(user.login)
                });
                io.to(socket.id).emit('RETURN_USERS', (usersList));
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    })

    socket.on('LOGIN_USER', (data) => {
        //Appel back pour récupérer l'utilisateur
        http.get('http://localhost:8083/auth?login='+data.login+'&pwd='+data.pwd, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            }); 
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                if (data!=""){
                    let user = JSON.parse(data);
                    let login = null;
                    login = user.login;
                    userConnected.push([login, socket.id]);
                    socket.username = data.username
                    //io.sockets.socket(socket.id).emit("LOGIN_PASSED",login);
                    io.to(socket.id).emit('LOGIN_PASSED',user);
                }
                else{
                    io.to(socket.id).emit('LOGIN_PASSED',null);
                }
                
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    })

    //listen on change_username
    socket.on('change_username', (data) => {
        socket.username = data.username;
    })

    //listen on change_destinataire
        socket.on('change_destinataire', (data) => {
            socket.destinataire = data.recipient;
        })

    //listen on new_message
    socket.on('SEND_MESSAGE', (data) => {
        //broadcast the new message
        console.log("SEND_MESSAGE " + data.recipient);
        var socketIdDest = null;
        userConnected.forEach(user => {
            if(user[0] == data.recipient){ 
                socketIdDest = user[1]
                console.log("emit message to " + socketIdDest);
                console.log(data);
                io.to(socketIdDest).emit('NEW_MESSAGE',{message : data.message, username : data.username, recipient : data.recipient });
            };
        })
        console.log("socket : " + socketIdDest);
        if(socketIdDest!=null){
            io.to(socket.id).emit('NEW_MESSAGE',{message : data.message, username : data.username, recipient : data.recipient });
        }
    })

    //listen on typing
    socket.on('typing', (data) => {
    	socket.broadcast.emit('typing', {username : socket.username, recipient : socket.recipient})
    })

    //disconnect 
    socket.on('disconnect', function() {
        console.log("disconnet, id : " + socket.id);
        userConnected = userConnected.filter(ele => ele[1] !== socket.id)
        console.log(userConnected);
    });

    //Listen on join game
    socket.on('JOIN_GAME',(data) => {
        console.log("username" + data.username);

        //Appel back pour récupérer l'utilisateur
        http.get('http://localhost:8083/userByLogin/'+data.username, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            }); 
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                if (data!=""){
                    console.log("user :");
                    if (waitingUser != null){
                        console.log("waitinguser existant, ajout waitinguser2");
                        waitingUser2 = JSON.parse(data);
                    }else {
                        console.log("waitinguser null, ajout waitinguser");
                        waitingUser = JSON.parse(data);
                    }
                }
                else{
                }
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
        
        setTimeout(startgame, 1500);

        function startgame(){
            console.log(userConnected);
            if (waitingUser != null && waitingUser2 != null){
                //Envoi start game au waitingUser2
                console.log("emit startgame to " + (socket.id)); 
                io.to(socket.id).emit('START_GAME',{User1 : waitingUser2, User2 : waitingUser});
                console.log("waitingUser.login" + waitingUser.login);
                userConnected.forEach(user => {
                    console.log(user[0]);
                    console.log(waitingUser.login);
                    if(user[0] == waitingUser.login){
                        socketIdDest = user[1]
                        console.log("emit startgame to " + socketIdDest); 
                        io.to(socketIdDest).emit('START_GAME',{User1 : waitingUser, User2 : waitingUser2});
                    };
                })
                waitingUser = null;
                waitingUser2 = null;
            }
        }
    })

    
})


