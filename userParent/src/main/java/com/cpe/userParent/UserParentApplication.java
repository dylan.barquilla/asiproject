package com.cpe.userParent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserParentApplication.class, args);
	}

}
