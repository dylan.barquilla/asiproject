package com.cpe.userDto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


public class UserDto implements Serializable {
    private static final long serialVersionUID = -1092250026124633551L;

    private Integer id;
    private String login;
    private String pwd;
    private float account;
    private String lastName;
    private String surName;
    private String email;

    private Set<Integer> CardIdList = new HashSet<>();

    public UserDto() {
        this.login = "";
        this.pwd = "";
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public UserDto(String login, String pwd) {
        super();
        this.login = login;
        this.pwd = pwd;
        this.lastName = "lastname_default";
        this.surName = "surname_default";
        this.email = "email_default";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Set<Integer> getCardList() {
        return CardIdList;
    }

    public void setCardList(Set<Integer> cardIdList) {
        this.CardIdList = cardIdList;
    }

    public void addAllCardList(Collection<Integer> cardList) {
        this.CardIdList.addAll(cardList);
    }


    public void addCard(Integer cardId) {
        this.CardIdList.add(cardId);
        //cardId.setUser(this);
    }

    private boolean checkIfCard(Integer c) {
        for (Integer c_c : this.CardIdList) {
            if (c_c == c) {
                return true;
            }
        }
        return false;
    }


    public float getAccount() {
        return account;
    }

    public void setAccount(float account) {
        this.account = account;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
