package com.cpe.cardParent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardParentApplication.class, args);
	}

}
